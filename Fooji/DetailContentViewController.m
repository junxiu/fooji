//
//  DetailContentViewController.m
//  Fooji
//
//  Created by Jun Xiu Chan on 1/13/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "DetailContentViewController.h"
#import <MapKit/MapKit.h>

@interface DetailContentViewController () <UIActionSheetDelegate>
@property (nonatomic, strong) FTGooglePlacesAPIDetailResponse *response;
@end

@implementation DetailContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAdditionalSetup];
    [self populateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

- (void)createAdditionalSetup {
    self.callButton.layer.cornerRadius = 4;
    self.routeButton.layer.cornerRadius = 4;
    self.detailPlaceContentView.layer.cornerRadius = 4;
}

- (void)populateData {
    FTGooglePlacesAPIDetailRequest *request = [[FTGooglePlacesAPIDetailRequest alloc] initWithPlaceId:self.detail.placeId];
    
    [FTGooglePlacesAPIService executeDetailRequest:request
                             withCompletionHandler:^(FTGooglePlacesAPIDetailResponse *response, NSError *error) {
                                 if (error) {
                                     //
                                 }
                                 else {
                                     CLLocationCoordinate2D position = response.location.coordinate;
                                     GMSMarker *marker = [GMSMarker markerWithPosition:position];
                                     marker.appearAnimation = kGMSMarkerAnimationPop;
                                     marker.map = _mapView;
                                     marker.title = response.name;
                                     marker.icon = [UIImage imageNamed:@"marker"];
                                     
                                     GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:response.location.coordinate.latitude
                                                                                             longitude:response.location.coordinate.longitude
                                                                                                  zoom:17];
                                     _mapView.camera = camera;
                                     
                                     NSString *placeContentStr = [NSString stringWithFormat:@"%@\n%@",response.name,response.formattedAddress];
                                     
                                     NSMutableAttributedString *placeContentMutStr = [[NSMutableAttributedString alloc] initWithString:placeContentStr];
                                     [placeContentMutStr addAttribute:NSFontAttributeName
                                                                value:[UIFont fontWithName:@"Nunito-Bold" size:15]
                                                                range:NSMakeRange(0, response.name.length)];
                                     
                                     self.detailPlaceLabel.attributedText = placeContentMutStr;
                                     
                                     self.ratingLabel.text = [NSString stringWithFormat:@"%.1g\n⭐️",response.rating];
                                     
                                     self.response = response;
                                 }
                             }];
}

- (void)call {
    NSString *phNo = [self.response.internationalPhoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",phNo]];
    
    [[UIApplication sharedApplication] openURL:phoneUrl];
}

- (void)openWebsite {
    [[UIApplication sharedApplication] openURL:self.response.websiteUrl];
}

- (IBAction)contact:(UIButton *)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Contact"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:self.response.internationalPhoneNumber,self.response.websiteUrl.absoluteString, nil];
    [actionSheet showInView:self.view];
}

- (IBAction)route:(id)sender {
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:self.response.location.coordinate addressDictionary:nil];
    MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
    item.name = self.response.name;
    [item openInMapsWithLaunchOptions:nil];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self call];
    }
    else {
        [self openWebsite];
    }
}

@end