//
//  MainViewController.m
//  Fooji
//
//  Created by Jun Xiu Chan on 1/12/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "MainViewController.h"
#import "PlaceCollectionViewCell.h"
#import "JCRBlurView.h"
#import "DetailViewController.h";
#import <CoreLocation/CoreLocation.h>

@interface MainViewController () <UICollectionViewDataSource, UICollectionViewDelegate, GMSMapViewDelegate, UITextFieldDelegate> {
    GMSMapView *_mapView;
    BOOL collectionViewIsScrolling;
    BOOL gotData;
    BOOL markerInfoWindowSelected;
    BOOL isShowingList;
    FTGooglePlacesAPISearchResultItem *selectedItem;
    NSTimer *timer;
}

@property (nonatomic, strong) NSArray *places;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAdditionalSetup];
    [self setupMap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

- (void)createAdditionalSetup {
    isShowingList = YES;
    gotData = NO;
    
    self.view.layer.cornerRadius = 4;
    
    collectionViewIsScrolling = NO;
    self.placesCollectionView.backgroundView = [[JCRBlurView alloc] initWithFrame:self.placesCollectionView.bounds];
    
    UIColor *color = [UIColor colorWithWhite:1 alpha:0.5f];
    self.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search nearby.." attributes:@{NSForegroundColorAttributeName: color}];
    
    [self.actionButton createBordersWithColor:[UIColor colorWithWhite:0 alpha:0.3f] withCornerRadius:CGRectGetHeight(self.actionButton.frame)/2 andWidth:2];
}

- (void)refreshNearbyPlaces {
    CLLocationCoordinate2D locationCoordinate = _mapView.myLocation.coordinate;
    
    FTGooglePlacesAPINearbySearchRequest *request = [[FTGooglePlacesAPINearbySearchRequest alloc] initWithLocationCoordinate:locationCoordinate];
    request.rankBy = FTGooglePlacesAPIRequestParamRankByDistance;
    request.types = @[@"restaurant",@"cafe",@"fast_food"];
    
    [FTGooglePlacesAPIService executeSearchRequest:request
                             withCompletionHandler:^(FTGooglePlacesAPISearchResponse *response, NSError *error) {
                                 if (error)
                                 {
                                     NSLog(@"Request failed. Error: %@", error);
                                 }
                                 else {
                                     _places = response.results;
                                     [self.placesCollectionView reloadData];
                                     [self refreshMap];
                                 }
                             }];
}

- (void)searchPlaces {
    collectionViewIsScrolling = YES;
    FTGooglePlacesAPITextSearchRequest *request = [[FTGooglePlacesAPITextSearchRequest alloc] initWithQuery:self.searchTextField.text];
    request.types = @[@"restaurant",@"cafe",@"fast_food"];
    request.locationCoordinate = _mapView.myLocation.coordinate;
    request.radius = 1000;
    
    [FTGooglePlacesAPIService executeSearchRequest:request
                             withCompletionHandler:^(FTGooglePlacesAPISearchResponse *response, NSError *error) {
                                 if (error)
                                 {
                                     NSLog(@"Request failed. Error: %@", error);
                                 }
                                 else {
                                     _places = response.results;
                                     NSLog(@"%@",_places);
                                     [self.placesCollectionView reloadData];
                                     [self refreshMap];
                                 }
                             }];
}

#pragma mark - UITextField

- (IBAction)textFieldDidChanged:(id)sender {
    if (self.searchTextField.text.length > 0) {
        if (timer.isValid) {
            [timer invalidate];
        }
        timer = [NSTimer timerWithTimeInterval:0.5f target:self selector:@selector(searchPlaces) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
    else {
        [timer invalidate];
        [self refreshNearbyPlaces];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Google Map

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    markerInfoWindowSelected = YES;
    selectedItem = marker.userData;
    [self performSegueWithIdentifier:kDetailVC sender:self];
}

- (void)refreshMap {
    for (FTGooglePlacesAPISearchResultItem *place in _places) {
        CLLocationCoordinate2D position = place.location.coordinate;
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = _mapView;
        marker.title = place.name;
        marker.snippet = place.addressString;
        marker.userData = place;
        marker.icon = [UIImage imageNamed:@"marker"];
    }
}

- (void)setupMap {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_mapView.myLocation.coordinate.latitude
                                                            longitude:_mapView.myLocation.coordinate.longitude
                                                                 zoom:19];
    
    _mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    _mapView.delegate = self;
    _mapView.alpha = 0;
    [self.view insertSubview:_mapView belowSubview:self.placesCollectionView];
    
    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    
    if (_mapView.alpha == 0) {
        [UIView animateWithDuration:0.85f
                              delay:0.5f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _mapView.alpha = 1;
                             _searchTextField.alpha = 1;
                         } completion:^(BOOL finished) {
                         }];
        
        
        self.actionButton.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
        [UIView animateWithDuration:0.55f
                              delay:0.8f
             usingSpringWithDamping:0.55f
              initialSpringVelocity:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.actionButton.alpha = 1;
                             self.actionButton.transform = CGAffineTransformIdentity;
                         } completion:^(BOOL finished) {
                         }];
    }
    
    if (!gotData) {
        gotData = YES;
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:17];
        [self refreshNearbyPlaces];
    }
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _places.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PlaceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlaceCollectionViewCell" forIndexPath:indexPath];
    [cell setData:(FTGooglePlacesAPISearchResultItem *)[_places objectAtIndex:indexPath.item] withCurrentLocation:_mapView.myLocation];
    
    if (!collectionViewIsScrolling) {
        [cell setFrameOriginY:cell.frame.origin.y + self.view.frame.size.height];
        
        [UIView animateWithDuration:0.85f
                              delay:0.05f * indexPath.item
             usingSpringWithDamping:1
              initialSpringVelocity:5
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [cell setFrameOriginY:cell.frame.origin.y - self.view.frame.size.height];
                         } completion:^(BOOL finished) {
                         }];
    }
        
    cell.tag = indexPath.item;

    [cell.placeLabel setFrameOriginX:63];
    [cell.placeLabel setFrameWidth:cell.frame.size.width - 93];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width, 100);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    collectionViewIsScrolling = YES; // isDragging is pre-defined but not isScrolling :(
    
    [UIView animateWithDuration:0.35f
                     animations:^{
                         if (scrollView.contentOffset.y + scrollView.contentInset.top >= 5) {
                             self.searchView.alpha = 1;
                         }
                         else {
                             self.searchView.alpha = 0;
                         }
                     }];
}

- (IBAction)triggerAction:(id)sender {
    if (isShowingList) {
        
        // show map
        
        isShowingList = NO;
        
        [UIView animateWithDuration:0.15f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.placesCollectionView.alpha = 0;
                             self.searchView.alpha = 0;
                             self.searchTextField.alpha = 0;
                             self.actionButton.backgroundColor = [UIColor colorWithRed:74/255.f green:144/255.f blue:226/255.f alpha:1];
                             [self.actionButton setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
                         } completion:^(BOOL finished) {
                         }];
    }
    else {
        
        // show list
        
        markerInfoWindowSelected = NO;
        isShowingList = YES;
        
        [UIView animateWithDuration:0.15f
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.placesCollectionView.alpha = 1;
                             self.searchTextField.alpha = 1;
                             self.actionButton.backgroundColor = [UIColor colorWithRed:255/255.f green:102/255.f blue:102/255.f alpha:1];
                             [self.actionButton setImage:[UIImage imageNamed:@"map"] forState:UIControlStateNormal];
                         } completion:^(BOOL finished) {
                         }];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:!isShowingList withAnimation:UIStatusBarAnimationFade];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kDetailVC]) {
        DetailViewController *detailVC = segue.destinationViewController;
        
        FTGooglePlacesAPISearchResultItem *data;
        
        if (markerInfoWindowSelected) {
            data = selectedItem;
        }
        else {
            NSInteger index = (long)[(NSIndexPath *)[self.placesCollectionView.indexPathsForSelectedItems firstObject] item];
            data = (FTGooglePlacesAPISearchResultItem *)[self.places objectAtIndex:index];
        }
        
        [detailVC setData:data];
    }
}
@end