//
//  DetailViewController.h
//  Fooji
//
//  Created by Jun Xiu Chan on 1/13/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (nonatomic, strong) FTGooglePlacesAPISearchResultItem *data;
@property (weak, nonatomic) IBOutlet UIView *detailContentView;
- (IBAction)dismiss:(id)sender;
@end
