//
//  PlaceCollectionViewCell.h
//  Fooji
//
//  Created by Jun Xiu Chan on 1/12/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EDStarRating/EDStarRating.h>
@interface PlaceCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIView *thatSneakyView;
- (void)setData:(FTGooglePlacesAPISearchResultItem *)data withCurrentLocation:(CLLocation *)curLocation;
@end
