//
//  DetailViewController.m
//  Fooji
//
//  Created by Jun Xiu Chan on 1/13/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailContentViewController.h"

@interface DetailViewController ()
@property (nonatomic, strong) FTGooglePlacesAPIDetailResponse *detail;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAdditionalSetup];
}

- (void)createAdditionalSetup {
    self.detailContentView.layer.cornerRadius = 4;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kDetailContentVC]) {
        DetailContentViewController *detailContentViewController = segue.destinationViewController;
        detailContentViewController.detail = self.data;
    }
}

- (IBAction)dismiss:(id)sender {
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         ((UIButton *)sender).alpha = 0;
                     }];
    
    UIImageView *blurScreenshotView = [self.view.subviews objectAtIndex:1];
    [UIView animateWithDuration:0.25f
                          delay:0.15f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         blurScreenshotView.alpha = 0;
                     }completion:^(BOOL finished) {
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
    
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:0.75f
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.detailContentView setFrameOriginY:CGRectGetMaxY(self.view.frame)];
                     } completion:^(BOOL finished) {
                         //
                     }];
}
@end