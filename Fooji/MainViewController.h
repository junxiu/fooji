//
//  MainViewController.h
//  Fooji
//
//  Created by Jun Xiu Chan on 1/12/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *placesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
- (IBAction)triggerAction:(id)sender;
- (IBAction)textFieldDidChanged:(id)sender;

@end
