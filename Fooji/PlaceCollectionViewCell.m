//
//  PlaceCollectionViewCell.m
//  Fooji
//
//  Created by Jun Xiu Chan on 1/12/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "PlaceCollectionViewCell.h"

@implementation PlaceCollectionViewCell

/*
 
 - (void)createBordersWithColor:(UIColor *)color
 withCornerRadius:(CGFloat)radius
 andWidth:(CGFloat)width;
 
 opacity:(CGFloat)opacity
 radius:(CGFloat)radius;
 */

- (void)awakeFromNib {
    [self.subContentView createBordersWithColor:[UIColor clearColor]
                               withCornerRadius:4
                                       andWidth:0];
    
    [self.thatSneakyView createBordersWithColor:[UIColor clearColor]
                               withCornerRadius:4
                                       andWidth:0];
    
    self.ratingLabel.layer.shadowColor = [UIColor colorWithRed:248/255.f green:231/255.f blue:28/255.f alpha:1].CGColor;
    self.ratingLabel.layer.shadowRadius = 5;
    self.ratingLabel.layer.shadowOffset = CGSizeZero;
    self.ratingLabel.layer.shadowOpacity = 0.35f;
}

- (void)setData:(FTGooglePlacesAPISearchResultItem *)data withCurrentLocation:(CLLocation *)curLocation{
    CGFloat rating = roundf(data.rating);
    
    NSMutableString *starStr = [NSMutableString string];
    
#warning Height is hardcoded
    
    if (rating > 0) {
        for (int i = 0; i < rating; i++) {
            [starStr appendString:@"⭐️"];
        }
        self.ratingLabel.text = starStr;
        [self.placeLabel setFrameHeight:67];
    }
    else {
        self.ratingLabel.text = @"";
        [self.placeLabel setFrameHeight:85];
    }
    
    CLLocationDistance distance = [curLocation getDistanceFrom:data.location];
    NSString *address;
    
    self.distanceLabel.text = [NSString stringWithFormat:@"%.1g\nkm",distance/1000];
    
    if (data.addressString.length == 0) {
        address = @"";
    }
    else {
        address = [NSString stringWithFormat:@"\n%@",data.addressString];
    }
    
    NSString *placeContentStr = [NSString stringWithFormat:@"%@\n%@",data.name,address];
    
    NSMutableAttributedString *placeContentMutStr = [[NSMutableAttributedString alloc] initWithString:placeContentStr];
    [placeContentMutStr addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:@"Nunito-Bold" size:15]
                               range:NSMakeRange(0, data.name.length)];
    
    self.placeLabel.attributedText = placeContentMutStr;
}

@end
