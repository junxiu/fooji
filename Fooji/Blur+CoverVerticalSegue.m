//
//  Blur+CoverVerticalSegue.m
//  Fooji
//
//  Created by Jun Xiu Chan on 1/13/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import "Blur+CoverVerticalSegue.h"

@implementation Blur_CoverVerticalSegue

- (void)perform {
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    UIImage *screenshotImage = [[sourceViewController.view imageByRenderingView] JTS_applyBlurWithRadius:2 tintColor:[UIColor colorWithWhite:0 alpha:0.5f] saturationDeltaFactor:1.8 maskImage:nil];
    
    UIImageView *blurScreenshotImageView = [[UIImageView alloc] initWithImage:screenshotImage];
    [blurScreenshotImageView setFrame:destinationViewController.view.frame];
    blurScreenshotImageView.alpha = 0;
    
    UIImageView *screenshotImageView = [[UIImageView alloc] initWithImage:[sourceViewController.view imageByRenderingView]];
    [screenshotImageView setFrame:destinationViewController.view.frame];
    
    [destinationViewController.view insertSubview:blurScreenshotImageView atIndex:0];
    [sourceViewController.view addSubview:destinationViewController.view];
    
#pragma mark - every view controller who uses this custom segue requires to mention the initial view to be presented with a tag '666'
    
    id firstViewToBePresented = [destinationViewController.view viewWithTag:666];
    [firstViewToBePresented setFrameOriginY:destinationViewController.view.frame.size.height];
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         blurScreenshotImageView.alpha = 1;
                     }completion:^(BOOL finished) {
                         [sourceViewController presentViewController:destinationViewController
                                                            animated:NO
                                                          completion:^{
                                                              [destinationViewController.view insertSubview:screenshotImageView atIndex:0];
                                                          }];
                     }];
    
    [UIView animateWithDuration:0.65f
                          delay:0.15f
         usingSpringWithDamping:0.75f
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [firstViewToBePresented setFrameOriginY:64];
                     } completion:^(BOOL finished) {
                         //
                     }];
}

@end
