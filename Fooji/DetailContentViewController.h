//
//  DetailContentViewController.h
//  Fooji
//
//  Created by Jun Xiu Chan on 1/13/15.
//  Copyright (c) 2015 Jun Xiu Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailContentViewController : UIViewController

@property (nonatomic, strong) FTGooglePlacesAPISearchResultItem *detail;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *routeButton;
@property (weak, nonatomic) IBOutlet UILabel *detailPlaceLabel;
@property (weak, nonatomic) IBOutlet UIView *detailPlaceContentView;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
- (IBAction)contact:(UIButton *)sender;
- (IBAction)route:(id)sender;

@end
